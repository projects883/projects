/**
 * @description main object for snake game
 * @type {{init: init, moveSnake: moveSnake, keyDownListener: keyDownListener, elem: Element, printTextTextElement: Element, isGameStarted: boolean, _gameLoopIntervalId: number, isOver: boolean}}
 */
var snake = {
    init: init,
    moveSnake: moveSnake,
    keyDownListener: keyDownListener,
    elem: document.getElementById("snake"),
    printTextTextElement: document.getElementById("print-text"),
    isGameStarted: false,
    _gameLoopIntervalId: 0,
    isOver: false
};
/**
 * @description initialization method for HTML5 snake game
 */
function init() {
    snake.isGameStarted = false;
    snake.isOver = false;
    snake.keyDownListener();

    _hideText();
    _gameLoop();

    console.log('snake is inited');
}
/**
 * @description it adds a keydown event listener
 */
function keyDownListener() {
    document.addEventListener('keydown', function (event) {
        event = event || window.event;
        snake._keycode = event.charCode || event.keyCode;

        snake.pozTop = snake.pozTop || 0;
        snake.pozLeft = snake.pozLeft || 0;

        _moveSnakeByLastKey();
    });
}
/**
* @description move the head of snake
* @returns {{left: left, right: right, up: up, down: down}}
* @private
*/
function moveSnake() {
    return {
        left: left,
        right: right,
        up: up,
        down: down
    };

    function left() {
        snake.pozLeft--;
        snake.elem.style['left'] = snake.pozLeft + 'px';
        _ifNeedGameOver();
    }

    function right() {
        snake.pozLeft++;
        snake.elem.style['left'] = snake.pozLeft + 'px';
        _ifNeedGameOver();

    }

    function up() {
        snake.pozTop--;
        snake.elem.style['top'] = snake.pozTop + 'px';
        _ifNeedGameOver();
    }

    function down() {
        snake.pozTop++;
        snake.elem.style['top'] = snake.pozTop + 'px';
        _ifNeedGameOver();
    }
}
/**
 * @private
 */
function _ifNeedGameOver() {
    // document.getElementById("alap").innerHTML = "end";
    if ( _isGameOver() ) {
        _drawText('GAME OVER');
        snake.isOver = true;
        snake._keycode = 0;
        clearInterval(snake._gameLoopIntervalId);
    }
}
/**
 *
 * @returns {boolean}
 * @private
 */
function _isGameOver() {
    return (
        snake.pozLeft < 0 ||
        snake.pozLeft > 400 ||
        snake.pozTop < 0 || snake.pozTop > 400
    );
}
/**
 *
 * @private
 */
function _gameLoop() {
    // if the default 0 value is the actual, make an interval
    if(!snake._gameLoopIntervalId) {
        snake._gameLoopIntervalId = setInterval(function () {
            console.log('game loop is running..');
            _moveSnakeByLastKey();
        }, 20);
    }
}
/**
 *
 * @private
 */
function _moveSnakeByLastKey() {
    // do nothing if game is over
    if(snake.isOver) {
        console.log('snake tries to move but game is over');
        return;
    }

    switch (snake._keycode) {
        case 37:
            snake.isGameStarted = true;
            snake.moveSnake().left();
            break;
        case 39:
            snake.isGameStarted = true;
            snake.moveSnake().right();
            break;
        case 38:
            snake.isGameStarted = true;
            snake.moveSnake().up();
            break;
        case 40:
            snake.isGameStarted = true;
            snake.moveSnake().down();
            break;
        default:
            return;
            break;
    }
}
/**
 *
 * @param text
 * @private
 */
function _drawText(text) {
    snake.printTextTextElement.style['display'] = 'block';
    snake.printTextTextElement.append(text);
}
/**
 * @description hide text
 * @private
 */
function _hideText() {
    snake.printTextTextElement.style['display'] = 'none';
}
